extends Area2D

var types = ["generator", "pumper", "compressor"]

var type
var firerate
var spread
var damage
var projectiles
var bullet_speed

onready var sprite = get_node("Sprite")

func _ready():
	randomize()
	var selection = randi()%5
	type = types[randi()%3]
	sprite.set_texture(load(str("res://weapons/",type,selection,".png")))
	init_weapon_stats()

func init_weapon_stats():
	if(type == "generator"):
		if(randf() < 0.2):
			firerate = rand_range(0.07, 0.2)
		else:
			firerate = rand_range(0.1, 0.6)
		spread = rand_range(1, 20)
		damage = rand_range(5, 50)
		projectiles = 1
		bullet_speed = rand_range(200, 450)
	elif(type == "pumper"):
		firerate = rand_range(1.0, 3.0)
		spread = rand_range(10, 30)
		damage = rand_range(10, 80)
		if(randf() < 0.3):
			projectiles = randi()%20 + 2
		elif(randf() < 0.2):
			projectiles = 1
			spread = rand_range(0, 10)
			damage = rand_range(200, 500)
			firerate = rand_range(1.0, 2.0)
		else:
			projectiles = randi()%10 + 1
		bullet_speed = rand_range(150, 400)
	elif(type == "compressor"):
		firerate = rand_range(0.5, 6.0)
		spread = rand_range(0, 3)
		if(randf() < 0.3):
			damage = rand_range(1000, 2000)
		else:
			damage = rand_range(200, 1000)
		if(randf() < 0.2):
			projectiles = randi()%3+1
			spread = rand_range(1, 5)
		else:
			projectiles = 1
		bullet_speed = rand_range(200, 500)

func pick_up():
	set_opacity(0)
	set_collision_mask_bit(0, 0)

func drop(pos):
	set_opacity(1)
	set_collision_mask_bit(0, 1)
	set_global_pos(pos)

func _on_Weapon_body_enter( body ):
	if(body.has_method("pick_up_weapon")):
		body.weapon_in_range = self
		body.is_weapon_in_range = true


func _on_Weapon_body_exit( body ):
	if(body.has_method("pick_up_weapon")):
		body.weapon_in_range = null
		body.is_weapon_in_range = false

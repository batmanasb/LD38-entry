#note: my implementation had an error that I couldn't find in time to I borrowed this one: 

#https://gitlab.com/TeddyDD/Godot-Cave-Generato/blob/master/generator.gd

#Copyright (c) 2015 Daniel Lewan - TeddyDD
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the 
#Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
#Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
#MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
#FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
#WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#################################################################################################################################################################################

#then I edited it to fit my project

extends Node2D

var player = preload("res://scenes/Player.tscn")
var mob = preload("res://scenes/Mob.tscn")
var weapon = preload("res://scenes/Weapon.tscn")
var boss = preload("res://scenes/Boss.tscn")
onready var holder = get_node("YSort")

onready var blocks = get_node("SmallTileMap")
onready var floors = get_node("LargeTileMap")
onready var global = get_tree().get_root().get_node("global")

const SIZE = Vector2( 100, 100)

var map = []
var steps = 3
var floor_size = int(32.0 * SIZE.x / 128.0 + 1.0)
var fill_percent = 38

var mob_spawns_remaining = 0
var weapon_spawns_remaining = 13

func _ready():
	randomize()
	mob_spawns_remaining = global.level * 7 + 5
	generate_tilemaps()
	generate_spawns()

	#fill floors
	for x in range(floor_size):
		for y in range(floor_size):
			floors.set_cell(x, y, global.level - 1)

func generate_tilemaps():
	_on_Random_pressed()
	for i in range(steps):
		_on_Smooth_pressed()
	update_map()

# Random button pressed
func _on_Random_pressed():
	map.resize(SIZE.x * SIZE.y)
	for y in range(SIZE.y):
		for x in range(SIZE.x):
			var i = y * SIZE.x + x # index of current tile
			
			# fill map with random tiles
			if randi() % 101 < fill_percent or x == 0 or x == SIZE.x - 1 or y == 0 or y == SIZE.y - 1:
				map[i] = 1 # wall
			else:
				map[i] = 0 # empty

# set tiles in Tilemap to match map array
func update_map():
	for y in range(SIZE.y):
		for x in range(SIZE.x):
			var i = y * SIZE.x + x
			if(map[i] == 1):
				blocks.set_cell(x, y, 0)

func _on_Smooth_pressed():
	# new map to apply changes
	var new_map = []
	new_map.resize(SIZE.x * SIZE.y)
	
	for e in range(map.size()): # copy old array
		new_map[e] = map[e]
	
	# we need to skip borders of screen
	for y in range(1,SIZE.y -1):
		for x in range(1,SIZE.x - 1):
			var i = y * SIZE.x + x
			if map[i] == 1: # if it was a wall
				if touching_walls(Vector2(x,y)) >= 4: # and 4 or more of its eight neighbors were walls
					new_map[i] = 1 # it becomes a wall
				else:
					new_map[i] = 0
			elif map[i] == 0: # if it was empty
				if touching_walls(Vector2(x,y)) >= 5: # we need 5 or neighbors
					new_map[i] = 1
				else:
					new_map[i] = 0
	map = new_map # apply new array
	
# return count of touching walls 
func touching_walls(point):
	var result = 0
	for y in [-1,0,1]:
		for x in [-1,0,1]:
			if x == 0 and y == 0: # we don't want to count tested point
				continue
			var i = (y + point.y) * SIZE.x + (x + point.x)
			if map[i] == 1:
				result += 1
	return result

func spawn_player(x,y):
	var node = player.instance()
	node.set_global_pos(Vector2(x*32,y*32))
	holder.add_child(node)

func spawn_boss(x,y):
	var node = boss.instance()
	node.set_global_pos(Vector2(x*32,y*32))
	holder.add_child(node)

func spawn_mob(x,y):
	var node = mob.instance()
	node.set_global_pos(Vector2(x*32,y*32))
	holder.add_child(node)

func spawn_weapon(x,y):
	var node = weapon.instance()
	node.set_global_pos(Vector2(x*32,y*32))
	holder.add_child(node)

func generate_spawns():
	var player_spawned = false
	while(not player_spawned):
		var pos = Vector2(randi() % int(SIZE.x), randi() % int(SIZE.y))
		if(blocks.get_cell(pos.x, pos.y) != 0):
			spawn_player(pos.x, pos.y)
			player_spawned = true
	while(mob_spawns_remaining > 0):
		var spawned = false
		while(not spawned):
			var pos = Vector2(randi() % int(SIZE.x), randi() % int(SIZE.y))
			if(blocks.get_cell(pos.x, pos.y) != 0):
				spawn_mob(pos.x, pos.y)
				spawned = true
				mob_spawns_remaining -= 1
	while(weapon_spawns_remaining > 0):
		var spawned = false
		while(not spawned):
			var pos = Vector2(randi() % int(SIZE.x), randi() % int(SIZE.y))
			if(blocks.get_cell(pos.x, pos.y) != 0):
				spawn_weapon(pos.x, pos.y)
				spawned = true
				weapon_spawns_remaining -= 1

func generate_boss_spawn():
	var spawned = false
	while(not spawned):
		var pos = Vector2(randi() % int(SIZE.x), randi() % int(SIZE.y))
		if(blocks.get_cell(pos.x, pos.y) != 0):
			spawn_boss(pos.x, pos.y)
			spawned = true






























#################################################################################################################
#my original code is below
#################################################################################################################



#
#map config
#var size = 100
#var birthLimit = 2
#var deathLimit = 3
#var initialChance = 0.1
#var steps = 4
#
#map vars
#var map = []
#var max_index = size - 1
#var floor_size = int(32.0 * size / 128.0 + 1.0)
#
#func _ready():
#	randomize()
#	generate_map()
#
#func generate_map():
#	initialize_map()
#	for i in range(steps):
#		run_simulation_step()
#	write_to_tilemap()
#
#func initialize_map():
#	map = []
#	for x in range(size):
#		map.append([])
#		for y in range(size):
#			map[x].append(randf() <= initialChance and x != 0 and x != max_index and y != 0 and y != max_index)
#
#func count_living_neighbors(x, y):
#	var count = 0
#	for i in range(-1,2):
#		for j in range(-1,2):
#			if(i == 0 and j == 0):
#				continue
#			if(map[x+i][y+i]):
#				count += 1
#	print(count)
#	return count
#
#func run_simulation_step():
#	var n_list = []
#	for i in range(size):
#		n_list.append([])
#		for j in range(size):
#			if(i == 0 or j == 0 or i == max_index or j == max_index):
#				n_list[i].append([])
#			else:
#				n_list[i].append(count_living_neighbors(i,j))
#	for x in range(1, max_index):
#		for y in range(1, max_index):
#			if(map[x][y]):
#				if(n_list[x][y] >= deathLimit):
#					map[x][y] = true
#				else:
#					map[x][y] = false
#			else:
#				if(n_list[x][y] >= birthLimit):
#					map[x][y] = true
#				else:
#					map[x][y] = false
#
#func write_to_tilemap():
#	for x in range(size):
#		for y in range(size):
#			if(map[x][y]):
#				blocks.set_cell(x, y, 0)
#	for x in range(floor_size):
#		for y in range(floor_size):
#			floors.set_cell(x, y, 3)
extends Control

onready var tie = get_node("text_interface_engine")
onready var global = get_tree().get_root().get_node("global")

func _ready():
	# Connect every signal to check them using the "print()" method
	tie.connect("input_enter", self, "_on_input_enter")
	tie.connect("buff_end", self, "_on_buff_end")
	tie.connect("state_change", self, "_on_state_change")
	tie.connect("enter_break", self, "_on_enter_break")
	tie.connect("resume_break", self, "_on_resume_break")
	tie.connect("tag_buff", self, "_on_tag_buff")

func _on_input_enter(s):
	print("Input Enter ",s)
	pass

func _on_buff_end():
	print("Buff End")
	global.resume()

func _on_state_change(i):
	print("New state: ", i)
	pass

func _on_enter_break():
	print("Enter Break")
	pass

func _on_resume_break():
	print("Resume Break")
	pass

func _on_tag_buff(s):
	print("Tag Buff ",s)
	pass

func play_dialog(n):
	tie.reset()
	tie.set_color(Color(1,1,1))
	if(n == 0):
		print("playing")
		# "A Beautiful Song"
		# by Henrique Alves
		tie.set_color(Color(1,1,1))
		# Buff text: "Text", duration (in seconds) of each letter
		tie.buff_text("This is a song, ", 0.1)
		tie.buff_text("lalala\n", 0.2)
		# Buff silence: Duration of the silence (in seconds)
		tie.buff_silence(1)
		tie.buff_text("It is so beautiful, ", 0.1)
		tie.buff_text("lalala\n", 0.2)
		tie.buff_silence(1)
		tie.buff_text("I love this song, ", 0.1)
		tie.buff_text("lalala\n", 0.2)
		tie.buff_silence(1)
		tie.buff_text("But now I'll ", 0.1) # WAIT FOR THE DROP
		tie.buff_text("DROP", 0.02) # ?????
		tie.buff_silence(0.4)
		tie.buff_text(" THE BASS\n", 0.02) # !!!!!
		tie.buff_silence(0.4)
		tie.buff_text("TVUVTUVUTUVU WOODBODBOWBDODBO TUUVUTVU WODWVDOOWDV WODOWVOOWDVOWVD DUBDUBDUBUDUDB OWVDOWVWDOWVDOWVDOWVDWVDOWVDOWVODV", 0.04) # I firmly believe this particular verse is my Magna Opus.
	
	if(n == 1):
		tie.buff_text("What is this?\n",0.1)
		tie.buff_text("..................\n",0.1)
		tie.buff_text("PREPARE TO DIE TINY HUMAN!!!",0.05)
		tie.buff_silence(10.0)
	if(n == 2):
		tie.buff_text("You again?!!!!!\n",0.1)
		tie.buff_text("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n",0.06)
		tie.buff_silence(10.0)
	if(n == 3):
		tie.buff_text("Hello again...\n",0.1)
		tie.buff_text("I guess it's",0.2)
		tie.buff_text("...",1.2)
		tie.buff_text(" A SMALL WORLD", 0.05)
		tie.buff_silence(10.0)
	if(n == 4):
		tie.buff_text("OMG\n",1.5)
		tie.buff_text("LOOK who's back!\n",0.1)
		tie.buff_text("This time I won't go easy on you",0.05)
		tie.buff_text("...",1.0)
		tie.buff_silence(10.0)
	if(n == 5):
		tie.buff_text("OMFG\n",2.0)
		tie.buff_text("How do you keep running into me?!\n",0.1)
		tie.buff_text("GO AWAY!!!",0.05)
		tie.buff_silence(10.0)
	if(n == 6):
		tie.buff_text("Oh shit, you're back!\n",0.05)
		tie.buff_text("..............\n",1.0)
		tie.buff_silence(10.0)
	if(n == 7):
		tie.buff_text("R.I.P.\n",2.0)
		tie.buff_silence(10.0)
	
	tie.set_state(tie.STATE_OUTPUT)
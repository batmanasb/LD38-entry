extends KinematicBody2D

onready var anim = get_node("Node2D/AnimationPlayer")
onready var anim_for_hits = get_node("Node2D/AnimationPlayerForHits")
onready var bullets = get_tree().get_root().get_node("Map/Bullets")
onready var map = get_tree().get_root().get_node("Map")
onready var global = get_tree().get_root().get_node("global")

var bullet = preload("res://scenes/MobBullet.tscn")
var explosion = preload("res://scenes/MobExplosion.tscn")

#Stats
var walkSpeed = 120.0
var health = 3000.0
var weapon_firerate = 1.0
var weapon_bullet_speed = 200.0
var weapon_bullet_spread = deg2rad(180)
var weapon_damage = 20.0
var weapon_projectiles = 50

#AI Stats
var attack_timing = [2, 10]
var attack_duration_timing = [2, 10]
var activity_timing = [0,10]
var move_timing = [1,10]
var move_speeds = [50, 120]

# Misc
var cooldown = 0.0

#AI
var activity_time
var time_elapsed = 0.0
var vel = Vector2()
var move_time = 0.0
var direction = 0.0
onready var target = get_tree().get_root().get_node("Map/YSort/Player")
var attack_time = 0.0
var attack_cooldown = 10.0
var attack_duration = 0.0

#makes sure it only dies once!
var once = true

func _ready():
	cutscene()

func start():
	randomize()
	activity_time = rand_range(1,6)
	set_fixed_process(true)

func cutscene():
	if(global.level == 1):
		anim.play("head tilt")
	elif(global.level == 2):
		anim.play("jaw drop")
	elif(global.level == 3):
		anim.play("upside down head")
	elif(global.level == 4):
		anim.play("grow eyes")
	elif(global.level == 5):
		anim.play("head spin")
	elif(global.level == 6):
		anim.play("turtle")
	elif(global.level == 7):
		anim.play("head explode")

func _fixed_process(delta):
	process_movement(delta)
	process_attack(delta)

# Move and slide along collision normal if the motion gets blocked
func move_and_slide(motion):
	if(anim.get_current_animation() != "walking"):
			anim.play("walking", 0.5, 2.0)
	
	# Save initial motion as the impulse in case of collision
	var impulse = motion
	
	# Keep remaining motion after a collision
	motion = move(motion)
	
	if(is_colliding()):
		var collider = get_collider()
		
		# Slide along the collision normal
		var n = get_collision_normal()
		motion = n.slide(motion)
		move(motion)
		
		vel = -vel

func fire_bullet():
	cooldown = weapon_firerate
	for i in range(weapon_projectiles):
		var node = bullet.instance()
		node.set_global_pos(get_global_pos())
		node.set_speed(weapon_bullet_speed)
		node.set_damage(weapon_damage)
		var dir = get_angle_to(target.get_global_pos())
		dir += rand_range(-weapon_bullet_spread, weapon_bullet_spread)
		node.set_direction(dir)
		bullets.add_child(node)

func apply_damage(damage):
	anim_for_hits.play("hit")
	health -= damage
	if(health <= 0):
		explode()

func explode():
	var node = explosion.instance()
	map.add_child(node)
	node.trigger(get_global_pos())
	set_collision_mask(0)
	set_layer_mask(0)
	set_fixed_process(false)
	hide()
	if(once):
		once = false
		global.next_level()

func process_movement(delta):
	if(move_time > 0.0):
		move_and_slide(vel * delta)
		move_time -= delta
	else:
		if(anim.get_current_animation() == "walking"):
			anim.play("idle", 0.5)
		time_elapsed += delta
		if(time_elapsed >= activity_time):
			activity_time = rand_range(activity_timing[0], activity_timing[1])
			time_elapsed = 0.0
			direction = rand_range(0, 360)
			vel = Vector2(0.0, walkSpeed).rotated(deg2rad(direction))
			move_time = rand_range(move_timing[0], move_timing[1])
			walkSpeed = rand_range(move_speeds[0], move_speeds[1])

func process_attack(delta):
	if(cooldown > 0.0):
		cooldown -= delta
	if(attack_duration > 0.0):
		attack_duration -= delta
		if(cooldown <= 0):
			fire_bullet()
		if(attack_duration <= 0.0):
			attack_time = rand_range(attack_timing[0], attack_timing[1])
			attack_cooldown = attack_time
	else:
		if(attack_cooldown > 0.0):
			attack_cooldown -= delta
		else:
			attack_duration = rand_range(attack_duration_timing[0], attack_duration_timing[1])
		
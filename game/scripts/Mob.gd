extends KinematicBody2D

onready var anim = get_node("AnimationPlayer")
onready var sprite_anim = get_node("Sprite/AnimationPlayer")
onready var bullets = get_tree().get_root().get_node("Map/Bullets")
onready var map = get_tree().get_root().get_node("Map")
onready var global = get_tree().get_root().get_node("global")

var bullet = preload("res://scenes/MobBullet.tscn")
var explosion = preload("res://scenes/MobExplosion.tscn")

#Stats
var walkSpeed = 100.0
var health = 100.0
var weapon_firerate = 1.0
var weapon_bullet_speed = 200.0
var weapon_bullet_spread = deg2rad(10)
var weapon_damage = 30.0
var weapon_projectiles = 1

#AI Stats
var attack_timing = [0, 10]
var attack_duration_timing = [0.5, 3]
var activity_timing = [0,10]
var move_timing = [1,10]
var move_speeds = [30, 100]

# Misc
var cooldown = 0.0

#AI
var activity_time
var time_elapsed = 0.0
var vel = Vector2()
var move_time = 0.0
var direction = 0.0
onready var target = get_tree().get_root().get_node("Map/YSort/Player")
var attack_time = 0.0
var attack_cooldown = 10.0
var attack_duration = 0.0

#makes sure it only dies once!
var once = true

func config(mob):
	if(mob == 0):
		get_node("Sprite").set_texture(load("res://mobs/beetle.png"))
		
		#Stats
		health = 50.0
		weapon_firerate = 2.0
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(15)
		weapon_damage = 20.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [5, 15]
		attack_duration_timing = [0, 1]
		activity_timing = [0, 3]
		move_timing = [1, 3]
		move_speeds = [30, 100]
	
	if(mob == 1):
		get_node("Sprite").set_texture(load("res://mobs/bigKitty.png"))
		
		#Stats
		health = 50.0
		weapon_firerate = 1.0
		weapon_bullet_speed = 50.0
		weapon_bullet_spread = deg2rad(5)
		weapon_damage = 20.0
		weapon_projectiles = 2
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [0, 3]
		activity_timing = [0, 1]
		move_timing = [1, 2]
		move_speeds = [100, 120]
	
	if(mob == 2):
		get_node("Sprite").set_texture(load("res://mobs/bigMouth.png"))
		
		#Stats
		health = 200.0
		weapon_firerate = 0.2
		weapon_bullet_speed = 100.0
		weapon_bullet_spread = deg2rad(5)
		weapon_damage = 5.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [2, 6]
		activity_timing = [10, 20]
		move_timing = [1, 4]
		move_speeds = [50, 150]
	
	if(mob == 3):
		get_node("Sprite").set_texture(load("res://mobs/crazy.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 1.0
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(10)
		weapon_damage = 40.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [30, 40]
		attack_duration_timing = [1, 2]
		activity_timing = [1, 3]
		move_timing = [1, 2]
		move_speeds = [30, 60]
	
	if(mob == 4):
		get_node("Sprite").set_texture(load("res://mobs/creeper.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 0.5
		weapon_bullet_speed = 100.0
		weapon_bullet_spread = deg2rad(180)
		weapon_damage = 20.0
		weapon_projectiles = 6
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [0.5, 2]
		activity_timing = [5, 15]
		move_timing = [1, 2]
		move_speeds = [20, 40]
	
	if(mob == 5):
		get_node("Sprite").set_texture(load("res://mobs/demon.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 0.1
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(7)
		weapon_damage = 5.0
		weapon_projectiles = 2
		
		#AI Stats
		attack_timing = [20, 40]
		attack_duration_timing = [1, 2]
		activity_timing = [1, 2]
		move_timing = [2, 4]
		move_speeds = [100, 110]
	
	if(mob == 6):
		get_node("Sprite").set_texture(load("res://mobs/dog.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 5.0
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(30)
		weapon_damage = 20.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [1, 2]
		attack_duration_timing = [100, 1000]
		activity_timing = [0, 1]
		move_timing = [0.5, 1]
		move_speeds = [30, 100]
	
	if(mob == 7):
		get_node("Sprite").set_texture(load("res://mobs/guardian.png"))
		
		#Stats
		health = 1000.0
		weapon_firerate = 0.1
		weapon_bullet_speed = 300.0
		weapon_bullet_spread = deg2rad(1)
		weapon_damage = 5.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [10, 20]
		activity_timing = [60, 120]
		move_timing = [0.5, 1]
		move_speeds = [10, 20]
	
	if(mob == 8):
		get_node("Sprite").set_texture(load("res://mobs/lobster.png"))
		
		#Stats
		health = 300.0
		weapon_firerate = 2.0
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(15)
		weapon_damage = 10.0
		weapon_projectiles = 5
		
		#AI Stats
		attack_timing = [15, 30]
		attack_duration_timing = [2, 6]
		activity_timing = [5, 10]
		move_timing = [1, 4]
		move_speeds = [50, 100]
	
	if(mob == 9):
		get_node("Sprite").set_texture(load("res://mobs/mantis.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 1.0
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(10)
		weapon_damage = 20.0
		weapon_projectiles = 2
		
		#AI Stats
		attack_timing = [5, 15]
		attack_duration_timing = [0.5, 1]
		activity_timing = [1, 2]
		move_timing = [1, 2]
		move_speeds = [120, 150]
	
	if(mob == 10):
		get_node("Sprite").set_texture(load("res://mobs/robber.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 1.0
		weapon_bullet_speed = 100.0
		weapon_bullet_spread = deg2rad(180)
		weapon_damage = 5.0
		weapon_projectiles = 2
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [20, 40]
		activity_timing = [1, 5]
		move_timing = [2, 4]
		move_speeds = [50, 60]
	
	if(mob == 11):
		get_node("Sprite").set_texture(load("res://mobs/robot.png"))
		
		#Stats
		health = 500.0
		weapon_firerate = 0.3
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(10)
		weapon_damage = 20.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [2, 3]
		activity_timing = [10, 20]
		move_timing = [1, 4]
		move_speeds = [50, 60]
	
	if(mob == 12):
		get_node("Sprite").set_texture(load("res://mobs/rumkin.png"))
		
		#Stats
		health = 50.0
		weapon_firerate = 0.5
		weapon_bullet_speed = 200.0
		weapon_bullet_spread = deg2rad(45)
		weapon_damage = 5.0
		weapon_projectiles = 10
		
		#AI Stats
		attack_timing = [20, 30]
		attack_duration_timing = [0.5, 2.0]
		activity_timing = [10, 20]
		move_timing = [1,7]
		move_speeds = [50, 100]
	
	if(mob == 13):
		get_node("Sprite").set_texture(load("res://mobs/samari.png"))
		
		#Stats
		health = 200.0
		weapon_firerate = 2.0
		weapon_bullet_speed = 50.0
		weapon_bullet_spread = deg2rad(20)
		weapon_damage = 20.0
		weapon_projectiles = 3
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [2, 6]
		activity_timing = [1, 2]
		move_timing = [4, 5]
		move_speeds = [90, 110]
	
	if(mob == 14):
		get_node("Sprite").set_texture(load("res://mobs/sentinal.png"))
		
		#Stats
		health = 100.0
		weapon_firerate = 5.0
		weapon_bullet_speed = 300.0
		weapon_bullet_spread = deg2rad(10)
		weapon_damage = 50.0
		weapon_projectiles = 1
		
		#AI Stats
		attack_timing = [10, 15]
		attack_duration_timing = [60, 360]
		activity_timing = [30, 60]
		move_timing = [10, 20]
		move_speeds = [10, 15]
	
	if(mob == 15):
		get_node("Sprite").set_texture(load("res://mobs/squid.png"))
		
		#Stats
		health = 70.0
		weapon_firerate = 0.1
		weapon_bullet_speed = 100.0
		weapon_bullet_spread = deg2rad(20)
		weapon_damage = 5.0
		weapon_projectiles = 10
		
		#AI Stats
		attack_timing = [15, 20]
		attack_duration_timing = [0.5, 1]
		activity_timing = [5, 10]
		move_timing = [1, 2]
		move_speeds = [100, 200]

func _ready():
	randomize()
	activity_time = rand_range(1,6)
	set_fixed_process(true)
	anim.play("MobAnimIdle", 0.5)
	config(randi()%17)

func _fixed_process(delta):
	process_movement(delta)
	process_attack(delta)

# Move and slide along collision normal if the motion gets blocked
func move_and_slide(motion):
	if(anim.get_current_animation() != "MobAnimWalking"):
			anim.play("MobAnimWalking", 0.5, 2.0)
	
	# Save initial motion as the impulse in case of collision
	var impulse = motion
	
	# Keep remaining motion after a collision
	motion = move(motion)
	
	if(is_colliding()):
		var collider = get_collider()
		
		# Slide along the collision normal
		var n = get_collision_normal()
		motion = n.slide(motion)
		move(motion)
		
		vel = -vel

func fire_bullet():
	cooldown = weapon_firerate
	for i in range(weapon_projectiles):
		var node = bullet.instance()
		node.set_global_pos(get_global_pos())
		node.set_speed(weapon_bullet_speed)
		node.set_damage(weapon_damage)
		var dir = get_angle_to(target.get_global_pos())
		dir += rand_range(-weapon_bullet_spread, weapon_bullet_spread)
		node.set_direction(dir)
		bullets.add_child(node)

func apply_damage(damage):
	sprite_anim.play("hit")
	health -= damage
	if(health <= 0):
		explode()

func explode():
	var node = explosion.instance()
	map.add_child(node)
	node.trigger(get_global_pos())
	set_collision_mask(0)
	set_layer_mask(0)
	set_fixed_process(false)
	hide()
	if(once):
		once = false
		global.record_kill()

func process_movement(delta):
	if(move_time > 0.0):
		move_and_slide(vel * delta)
		move_time -= delta
	else:
		if(anim.get_current_animation() == "MobAnimWalking"):
			anim.play("MobAnimIdle", 0.5)
		time_elapsed += delta
		if(time_elapsed >= activity_time):
			activity_time = rand_range(activity_timing[0], activity_timing[1])
			time_elapsed = 0.0
			direction = rand_range(0, 360)
			vel = Vector2(0.0, walkSpeed).rotated(deg2rad(direction))
			move_time = rand_range(move_timing[0], move_timing[1])
			walkSpeed = rand_range(move_speeds[0], move_speeds[1])

func process_attack(delta):
	if(cooldown > 0.0):
		cooldown -= delta
	if(attack_duration > 0.0):
		attack_duration -= delta
		if(cooldown <= 0):
			fire_bullet()
		if(attack_duration <= 0.0):
			attack_time = rand_range(attack_timing[0], attack_timing[1])
			attack_cooldown = attack_time
	else:
		if(attack_cooldown > 0.0):
			attack_cooldown -= delta
		else:
			attack_duration = rand_range(attack_duration_timing[0], attack_duration_timing[1])
		
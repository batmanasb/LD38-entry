extends Node

var level = 1
var kills_required = 8
var kills_per_level = 6
var kills = 0
var is_boss_active = false

func _ready():
	randomize()

func restart():
	get_tree().reload_current_scene()

func record_kill():
	kills += 1
	if(kills >= kills_required and not is_boss_active):
		spawn_boss()
	get_tree().get_root().get_node("Map/YSort/Player").update_hud()

func spawn_boss():
	is_boss_active = true
	get_tree().get_root().get_node("Map").generate_boss_spawn()
	get_tree().set_pause(true)
	get_tree().get_root().get_node("Map/CanvasLayer/Panel").show()
	get_tree().get_root().get_node("Map/CanvasLayer/Panel").play_dialog(level)
	get_tree().get_root().get_node("Map/YSort/Player").set_light_status(false)

func get_kills_remaining():
	if(is_boss_active):
		return "The Boss!"
	return str(kills_required - kills)

func next_level():
	is_boss_active = false
	level += 1
	kills_required = level * kills_per_level
	kills = 0
	print(str("level = ",level))
	restart()

func resume():
	get_tree().set_pause(false)
	get_tree().get_root().get_node("Map/YSort/Player").set_light_status(true)
	get_tree().get_root().get_node("Map/YSort/Player/Camera2D").make_current()
	get_tree().get_root().get_node("Map/CanvasLayer/Panel").hide()
	get_tree().get_root().get_node("Map/YSort/Boss").start()
	
	if(level == 7):
		get_tree().quit()

func on_death():
	is_boss_active = false
	kills = 0
	restart()
extends Particles2D

func _ready():
	set_process(true)

func trigger(pos):
	set_global_pos(pos)
	set_emitting(true)

func _process(delta):
	if(not is_emitting()):
		queue_free()
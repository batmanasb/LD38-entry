extends Area2D

var damage = 30.0
var speed = 0.0
var vel = Vector2()

onready var map = get_tree().get_root().get_node("Map")
var explosion = preload("res://scenes/MobBulletExplosion.tscn")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	var new_pos = get_pos() + vel * delta
	set_pos(new_pos)

func set_direction(dir):
	vel = vel.rotated(dir)

func set_speed(speed):
	self.speed = speed
	vel = Vector2(0.0, speed)

func set_damage(damage):
	self.damage = damage

func _on_Bullet_body_enter( body ):
	if(body.has_method("apply_damage")):
		body.apply_damage(damage)
	explode()

func explode():
	var node = explosion.instance()
	map.add_child(node)
	node.trigger(get_global_pos())
	set_collision_mask(0)
	set_layer_mask(0)
	queue_free()

#for player bullet and mob bullet collisions
func _on_Bullet_area_enter( area ):
	explode()
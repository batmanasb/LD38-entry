extends KinematicBody2D

onready var text = get_tree().get_root().get_node("Map/CanvasLayer/Panel")
onready var anim = get_node("AnimationPlayer")
onready var sprite_anim = get_node("Sprite/AnimationPlayer")
onready var bullets = get_tree().get_root().get_node("Map/Bullets")
onready var global = get_tree().get_root().get_node("global")
onready var hud = get_tree().get_root().get_node("Map/CanvasLayer/HUD")
onready var light = get_node("Light2D")
var bullet = preload("res://scenes/Bullet.tscn")

#Stats
var walkSpeed = 100.0
var health = 100.0
var weapon_firerate = 1.0
var weapon_bullet_speed = 200.0
var weapon_bullet_spread = deg2rad(1)
var weapon_damage = 30.0
var weapon_projectiles = 1

# Misc
var cooldown = 0.0
var weapon_in_range = null
var is_weapon_in_range = false
var has_current_weapon = false
var current_weapon = null
var let_go_of_pickup = true

# Direction angles
const upAngle = deg2rad(180)
const downAngle = deg2rad(0)
const leftAngle = deg2rad(-90)
const rightAngle = deg2rad(90)
const upLeftAngle = deg2rad(-135)
const upRightAngle = deg2rad(135)
const downLeftAngle = deg2rad(-45)
const downRightAngle = deg2rad(45)
const angle_adjustment_magic_number = deg2rad(60)

# Velocity
var vel = Vector2(0.0, walkSpeed)
var upVel = vel.rotated(upAngle)
var downVel = vel.rotated(downAngle)
var leftVel = vel.rotated(leftAngle)
var rightVel = vel.rotated(rightAngle)
var upLeftVel = vel.rotated(upLeftAngle)
var upRightVel = vel.rotated(upRightAngle)
var downLeftVel = vel.rotated(downLeftAngle)
var downRightVel = vel.rotated(downRightAngle)
var zeroVel = Vector2()

func _ready():
	set_fixed_process(true)
	set_process_input(true)
	anim.play("idle", 0.5)
	update_hud()

func _input(event):
	if(Input.is_action_pressed("interact")):
		if(let_go_of_pickup):
			if(is_weapon_in_range):
				if(not Input.is_action_pressed("fire")):
					pick_up_weapon()
					let_go_of_pickup = false
	else:
		let_go_of_pickup = true
	if(Input.is_action_pressed("restart")):
		global.on_death()

func _fixed_process(delta):
	process_movement(delta)
	process_attacks(delta)

# Move and slide along collision normal if the motion gets blocked
func move_and_slide(motion):
	if(anim.get_current_animation() != "walking"):
			anim.play("walking", 0.5, 2.0)
	
	# Save initial motion as the impulse in case of collision
	var impulse = motion
	
	# Keep remaining motion after a collision
	motion = move(motion)
	
	if(is_colliding()):
		var collider = get_collider()
		
		# Slide along the collision normal
		var n = get_collision_normal()
		motion = n.slide(motion)
		move(motion)

func process_movement(delta):
	if(Input.is_action_pressed("up") and Input.is_action_pressed("left") and not Input.is_action_pressed("down") and not Input.is_action_pressed("right")):
		move_and_slide(upLeftVel*delta)
	elif(Input.is_action_pressed("up") and Input.is_action_pressed("right") and not Input.is_action_pressed("down") and not Input.is_action_pressed("left")):
		move_and_slide(upRightVel*delta)
	elif(Input.is_action_pressed("down") and Input.is_action_pressed("left") and not Input.is_action_pressed("up") and not Input.is_action_pressed("right")):
		move_and_slide(downLeftVel*delta)
	elif(Input.is_action_pressed("down") and Input.is_action_pressed("right") and not Input.is_action_pressed("up") and not Input.is_action_pressed("left")):
		move_and_slide(downRightVel*delta)
	elif(Input.is_action_pressed("up") and not Input.is_action_pressed("down")):
		move_and_slide(upVel * delta)
	elif(Input.is_action_pressed("down") and not Input.is_action_pressed("up")):
		move_and_slide(downVel * delta)
	elif(Input.is_action_pressed("left") and not Input.is_action_pressed("right")):
		move_and_slide(leftVel * delta)
	elif(Input.is_action_pressed("right") and not Input.is_action_pressed("left")):
		move_and_slide(rightVel * delta)
	else:
		if(anim.get_current_animation() == "walking"):
			anim.play("idle", 0.5)

func process_attacks(delta):
	if(cooldown > 0.0):
		cooldown -= delta
	if(Input.is_action_pressed("fire") and cooldown <= 0.0):
		fire_bullet()
	if(Input.is_action_pressed("alt fire")):
		pass

func fire_bullet():
	cooldown = weapon_firerate
	for i in range(weapon_projectiles):
		var node = bullet.instance()
		node.set_global_pos(get_global_pos())
		node.set_speed(weapon_bullet_speed)
		node.set_damage(weapon_damage)
		var dir = get_angle_to(get_global_mouse_pos())
		dir += rand_range(-weapon_bullet_spread, weapon_bullet_spread)
		node.set_direction(dir)
		bullets.add_child(node)
	
func pick_up_weapon():
	if(is_weapon_in_range):
		weapon_in_range.pick_up()
		weapon_firerate = weapon_in_range.firerate
		weapon_bullet_spread = deg2rad(weapon_in_range.spread)
		weapon_bullet_speed = weapon_in_range.bullet_speed
		weapon_damage = weapon_in_range.damage
		weapon_projectiles = weapon_in_range.projectiles
		if(has_current_weapon):
			current_weapon.drop(get_global_pos())
		current_weapon = weapon_in_range
		is_weapon_in_range = false
		weapon_in_range = null
		has_current_weapon = true

func apply_damage(damage):
	sprite_anim.play("MobHitAnim")
	health -= damage
	if(health <= 0):
		global.on_death()
	update_hud()

func update_hud():
	hud.set_text(str("Kills remaining: ",global.get_kills_remaining(),"\n","Health: ",health,"\n","Level: ",global.level))

func set_light_status(status):
	if(status):
		light.show()
	else:
		light.hide()